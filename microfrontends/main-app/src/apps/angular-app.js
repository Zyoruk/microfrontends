import * as singleSpa from 'single-spa';
import { matchingPathname, runScript } from './utils';

const loadAngularApp = async () => {
	await runScript('http://localhost:8085/angular/inline.bundle.js');
	await runScript('http://localhost:8085/angular/polyfills.bundle.js');
	await runScript('http://localhost:8085/angular/styles.bundle.js');
	await runScript('http://localhost:8085/angular/vendor.bundle.js');
	await runScript('http://localhost:8085/angular/main.bundle.js');
	return window.angularApp;
};

export const registerAngularApp = () => {
	singleSpa.registerApplication(
		'angular-app',
		loadAngularApp,
		matchingPathname(['/angular', '/'])
	);
};
