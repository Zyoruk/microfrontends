### Microfrontends

This project is based on a GitLab [Project](https://github.com/Pragmatists/microfrontends/), and it seeks to put it all together using containers in order to have a containerized microfrontends POC. It also has Kubernetes services and pods.

